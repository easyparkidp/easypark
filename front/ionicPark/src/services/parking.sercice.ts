import { Injectable } from '@angular/core';

@Injectable()
export class ParkingService {

    constructor() { }

    getParkings(): Array<any> {
        let parkings = [
            {
                name: "Parking Autocité Euralille",
                latitude: 50.6367287,
                longitude: 3.073383499999977
            },
            {
                name: "Parking Zenpark",
                latitude: 50.62968069999999,
                longitude: 3.0231699999999364
            },
            {
                name: "Champ de mars",
                latitude: 50.64002730000001,
                longitude: 3.0511699999999564
            },
            {
                name: "Parking Zenpark",
                latitude: 50.6350521,
                longitude: 3.0583335000000034
            },
            {
                name: "parking plaza public",
                latitude: 50.6342799,
                longitude: 3.0312880999999834
            },
            {
                name: "Parking EFFIA Nouveau Siècle",
                latitude: 50.637137,
                longitude: 3.0601245999999946
            },
           
        ];
        return parkings;
    }

}