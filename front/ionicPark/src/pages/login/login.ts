import { ListePage } from './../liste/liste';
import { ParkingPage } from './../parking/parking';
import { User } from './../../models/user';
import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, App } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  user = {} as User;
  retour: any;
  constructor(public navCtrl: NavController, public app: App, private http: HttpClient, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  async loginUser(user: User) {
    //Api connections : if success redirect to TabsPage 
    // this.navCtrl.push(TabsPage);

    if (this.user.email != this.user.password && (this.user.email != null || this.user.password != null)) {

      const formData: FormData = new FormData();
      formData.append('email', this.user.email);
      formData.append('password', this.user.password);
      this.http.post('http://localhost:8080/authentifier', formData).subscribe(r => {
        console.log(r);
        this.retour = r;
      });
      this.navCtrl.push(ListePage);



    } else {

      let alert = this.alertCtrl.create({
        title: 'Mauvais Mot de passe',
        subTitle: 'Veuillez retaper votre mdp',
        buttons: ['OK']
      });
      alert.present();

    }
  }
  logout() {
    //Api Token logout
    const root = this.app.getRootNav();
    root.popToRoot();
  }
}
