import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { GpsPage } from '../gps/gps';

/**
 * Generated class for the ParkingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-parking',
  templateUrl: 'parking.html',
})
export class ParkingPage {
  items:any;
  retour: any;

  
  constructor(public navCtrl: NavController,  private http: HttpClient) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParkingPage');
    this.http.get('http://localhost:8080/allparking').subscribe(a => {
     console.log(a);
     this.items=a;
     console.log(this.items);
    });
  }


  itemSelected(item: string) {
    console.log("Selected Item", item);
    const formData: FormData = new FormData();
    formData.append('nom', item);
    this.http.post('http://localhost:8080/park', formData).subscribe(r => { // à envoyer au back pour recevoir les infos du parking
      console.log("resultate d'apel", r);
      this.retour = r;

      this.navCtrl.push(GpsPage, r);
    });
  }
}
