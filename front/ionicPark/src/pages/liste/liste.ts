import { HomePage } from './../home/home';
import { SignupPage } from './../signup/signup';
import { WelcomePage } from './../welcome/welcome';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { ParkingService } from '../../services/parking.sercice';


declare var google;

/**
 * Generated class for the ListePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-liste',
  templateUrl: 'liste.html',
})
export class ListePage {
  @ViewChild('map') mapElement: ElementRef;
  private map: any;
  public userMarker: any;
  private parkingMarkers = new Array();

  constructor(public navCtrl: NavController, public navParams: NavParams, private geolocation: Geolocation, private parkingService: ParkingService) {
  }

  ionViewDidLoad() {
    this.loadMap();
  }
  /**
   * création de la carte google
   */
  loadMap() {
    let mapOptions = {
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addParkingMarker();
    this.centerMap();
  }

  /**
   * Géolocalisation de l'utilisateur
   * affichage du marker et centrage de la carte
   */
  centerMap() {
    navigator.geolocation.getCurrentPosition(
      (pos) => {
        // récup. des coords
        let coord = pos.coords;
        // transformation des coordonnées Ionic en coordonnées GoogleMap
        let latLng = new google.maps.LatLng(coord.latitude, coord.longitude);
        // centrage
        this.map.setCenter(latLng);
        // ajout du marqueur
        this.userMarker = new google.maps.Marker({
          position: latLng,
          map: this.map
        });
        // TODO à deplacer ailleurs, mais bon...
        let closestMarker = this.findClosestParking();
        this.calculateAndDisplayRoute(this.userMarker.position, closestMarker.position);
      },
      (err) => {
        console.warn(`ERROR(${err.code}): ${err.message}`);
      },
      {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
      }
    );
  }
  /**
   * on récupère la liste des parkings depuis parkingService
   * nous plaçons les markers sur la carte pour chacun des parkings
   */
  addParkingMarker() {
    let parkings = this.parkingService.getParkings();
    // création des markers pour chacun des parkings
    parkings.forEach(parking => {
      let latLng = new google.maps.LatLng(parking.latitude, parking.longitude);

      let icon = {
        url: '../assets/imgs/parking.svg',
        fillColor: '#FF0000',
        fillOpacity: .6,
        scaledSize: new google.maps.Size(50, 50), // scaled size
        strokeWeight: 0,
        scale: .25
      }

      let marker = new google.maps.Marker({
        position: latLng,
        map: this.map,
        icon: icon,
        title: parking.name
      });

      this.parkingMarkers.push(marker);

    });
  }
  
  

  /**
   * trouve le parking le plus proche de l'utilisateur
   */
  private findClosestParking() {
    let userLatLng = this.userMarker.getPosition();

    /*
     * on utilise un reducer, une fonction qui permet de comparer les différents éléments d'une liste
     * pour trouver le parking le plus proche, nous utilisons Pythagore
     * prev est le dernier parking plus proche
     * curr est le parking à tester pour récupérer sa distance de l'utilisateur
     */
    let closestParkingMarker = this.parkingMarkers.reduce(function (prev, curr) {
      // currentABBC = (x1-x2)² + (y1-y2)²
      // currentDist = √currentABBC soit √(x1-x2)² + (y1-y2)² soit l'hypotenus
      let currentABBC = Math.pow((curr.position.lat() - userLatLng.lat()), 2) + Math.pow((curr.position.lng() - userLatLng.lng()), 2);
      let currentDist = Math.sqrt(currentABBC);


      let previousABBC = Math.pow((prev.position.lat() - userLatLng.lat()), 2) + Math.pow((prev.position.lng() - userLatLng.lng()), 2);
      let previousDist = Math.sqrt(previousABBC);

      // si la distance du marqueur courant est plus proche on retourne le marqueur courant sinon le précédent marqueur
      return currentDist < previousDist ? curr : prev;
    });
    // on retourne le parking le plus proche
    return closestParkingMarker;
  }

  /**
   * calcule et affiche la route entre l'utilisateur et le parking
   */
  calculateAndDisplayRoute(origin, destination) {
    // Service de calcul d'itinéraire
    let directionsService = new google.maps.DirectionsService;
    let directionsDisplay = new google.maps.DirectionsRenderer;

    directionsDisplay.setMap(this.map);

    let req = {
      origin: origin,
      destination: destination,
      travelMode: google.maps.DirectionsTravelMode.DRIVING // Type de transport
    }

    directionsService.route(req, (response, status) => { // Envoie de la requête pour calculer le parcours
      if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response); // Trace l'itinéraire sur la carte et les différentes étapes du parcours
      }
    });
  }
  
    versParkingV2() {
      this.navCtrl.push(WelcomePage);
    }
  
    versParkingEura() {
      this.navCtrl.push(SignupPage);
    }
  
    versParkingCarre() {
      this.navCtrl.push(HomePage);
    }
}