import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListePage } from './liste';
import { ParkingService } from '../../services/parking.sercice';

@NgModule({
  declarations: [
    ListePage,
  ],
  imports: [
    IonicPageModule.forChild(ListePage),
  ],
  exports: [
    ListePage
  ],
  providers: [
    ParkingService
  ]
})
export class ListePageModule {}
