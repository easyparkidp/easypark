import { Component } from '@angular/core';
import { ListePage } from '../liste/liste';

import { NavController, App } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


 versParking(){
   this.navCtrl.push(ListePage);
 }
  constructor(public navCtrl: NavController, public app: App) {

  }
  logout() {
    //Api Token logout
    const root = this.app.getRootNav();
    root.popToRoot();
  }
}
