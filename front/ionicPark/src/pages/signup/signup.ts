import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, App } from 'ionic-angular';
import { UserRegister } from '../../models/UserRegister';
import { HttpClient } from '@angular/common/http';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  retour: any;

  userRegister = {} as UserRegister;
  constructor(public navCtrl: NavController, public navParams: NavParams, public app: App, private http: HttpClient, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  async signupRegister(userRegister: UserRegister) {
    //Api connections : if success redirect to TabsPage 
    // this.navCtrl.push(TabsPage);
    if (this.userRegister.email == this.userRegister.email2 && this.userRegister.password == this.userRegister.password2 && this.userRegister.email == this.userRegister.password && this.userRegister.nom != null && this.userRegister.prenom != null && this.userRegister.email != null && this.userRegister.password != null && this.userRegister.email2 != null && this.userRegister.password2 != null) {
      console.log(userRegister);

      const formData: FormData = new FormData();
      formData.append('nom', this.userRegister.nom);
      formData.append('prenom', this.userRegister.prenom);
      formData.append('email', this.userRegister.email);
      formData.append('password', this.userRegister.password);
      this.http.post('http://localhost:8080/enregistrement', formData).subscribe(r => {
        console.log(r);
        this.retour = r;

      });


      this.navCtrl.push(LoginPage);

    } else {
      let alert = this.alertCtrl.create({
        title: 'Mauvais Mot de passe',
        subTitle: 'Veuillez retaper votre mdp',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  logout() {
    //Api Token logout
    const root = this.app.getRootNav();
    root.popToRoot();
  }
}
