import { GpsPage } from './../pages/gps/gps';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { TabsPage } from '../pages/tabs/tabs';
import { ListePage } from '../pages/liste/liste';

// plugins 
import { Geolocation } from '@ionic-native/geolocation';
import { ListePageModule } from '../pages/liste/liste.module';
import { HomePage } from '../pages/home/home';
import { WelcomePage } from "../pages/welcome/welcome";
import { LoginPage } from "../pages/login/login";
import { SignupPage } from "../pages/signup/signup";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClientModule } from '@angular/common/http';
import { ParkingPage } from '../pages/parking/parking';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
        HomePage,
    WelcomePage,
    LoginPage,
    SignupPage,
    GpsPage,
    ParkingPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    ListePageModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    ListePage, 
    HomePage,
    WelcomePage,
    LoginPage,
    SignupPage,
    GpsPage,
    ParkingPage
  ],
  providers: [
    StatusBar,
    Geolocation,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
