package lib.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import lib.entities.Parking;

@RepositoryRestResource(collectionResourceRel = "Parking", path = "parking")
public interface ParkingRepository extends JpaRepository<Parking, Integer> {
	// List<Parking> findByNom(@Param(value = "nom") String Nom);

	@Query("select park from Parking park where park.nom=:nom")
	public Parking getByNom(@Param(value = "nom") String nom);

	@Override
	@Query("select park from Parking park")
	public List<Parking> findAll();
}
