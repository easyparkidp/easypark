package lib.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import lib.entities.Place;

@RepositoryRestResource(collectionResourceRel = "Place", path = "place")
public interface PlaceRepository extends JpaRepository<Place, Integer> {

	@Query("SELECT pl FROM Place pl INNER JOIN pl.parking park WHERE park.idParking=:id_parking AND pl.empty=:empty")
	List<Place> findByEmpty(@Param(value = "empty") boolean empty, @Param(value = "id_parking") int parking);

}
