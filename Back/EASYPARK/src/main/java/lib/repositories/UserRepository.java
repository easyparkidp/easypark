package lib.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import lib.entities.User;

@RepositoryRestResource(collectionResourceRel = "User", path = "user")
public interface UserRepository extends JpaRepository<User, Integer> {

	// 1ere methode
	// List<User> findByNom(@Param(value = "nom") String Nom);

	// 2eme methode
	// @Query("select u from User u where u.nom = ?1")
	// List<User> findByNom(@Param(value = "nom") String nom);

	// 3eme methode
	// exemple:
	// http://localhost:8080/user/search/findByNomAndEmail?nom=Ainouch&email=khalid.ainouch@gmail.com
	//
	// @Query("select u from User u where u.nom= ?1 AND u.email = ?2")
	// public User findByNomAndEmail(@Param(value = "nom") String nom, @Param(value
	// = "email") String email);
	@Query("SELECT CASE WHEN COUNT(u) > 0 THEN true ELSE false END FROM User u WHERE u.email=:email")
	boolean existsByEmail(@Param("email") String email);

	// methode d'authentification : SELECT * FROM USER WHERE email = valEmail And
	// mdP = valMdP

	@Query("SELECT u FROM User u WHERE u.email= :excpectEmail AND u.password = :excpectMdP")
	public User authentifier(@Param("excpectEmail") String email, @Param("excpectMdP") String mdP);
}
