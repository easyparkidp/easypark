package lib.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import lib.entities.Parking;
import lib.repositories.ParkingRepository;

@CrossOrigin
@RequestMapping("/")
@Controller
public class ParkingController {
	@Autowired
	private ParkingRepository pr;
	private final static Log LOG = LogFactory.getLog(ParkingController.class);

	@ResponseBody
	@RequestMapping(value = "/park", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Parking envoyer(@RequestParam("nom") String nom) {
		ParkingController.LOG.info("passage dans envoyer vers front " + nom);
		if (!nom.trim().isEmpty()) {
			Parking p = this.pr.getByNom(nom);
			ParkingController.LOG.info("test message parking " + p);
			return p;
		} else {
			return null;
		}
	}

	@ResponseBody
	@RequestMapping(value = "/allparkings", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Parking> getAllParkings(@PathVariable("nom") String nom) {
		ParkingController.LOG.info("envoyer le nom des parking " + nom);
		List<Parking> nomParkings = this.pr.findAll();
		ParkingController.LOG.info("test message parking " + nomParkings);
		return nomParkings;
	}

}
