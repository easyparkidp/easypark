package lib.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import lib.entities.Place;
import lib.repositories.PlaceRepository;

@CrossOrigin
@RequestMapping("/")
@Controller
public class PlaceController {

	private PlaceRepository pl;
	private final static Log LOG = LogFactory.getLog(ParkingController.class);

	public PlaceRepository getPl() {
		return this.pl;
	}

	@Autowired
	public void setPl(PlaceRepository pl) {
		this.pl = pl;
	}

	@ResponseBody
	@RequestMapping(value = "/places", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Place> getAllPlacesEmpty(@RequestParam("empty") boolean empty,
			@RequestParam("id_parking") int idparking) {
		PlaceController.LOG.info("Passage des places avec " + empty + " " + idparking);
		List<Place> places = this.pl.findByEmpty(empty, idparking);
		PlaceController.LOG.info("test message places " + places);
		return places;

	}

}