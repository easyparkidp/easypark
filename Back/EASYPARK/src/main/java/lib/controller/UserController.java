package lib.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import lib.entities.User;
import lib.repositories.UserRepository;

@CrossOrigin
@Controller
public class UserController {

	@Autowired
	private UserRepository ur;
	private final static Log LOG = LogFactory.getLog(UserController.class);

	@ResponseBody
	@RequestMapping(value = "/authentifier", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public User authentify(@RequestParam("email") String email, @RequestParam("password") String password) {
		UserController.LOG.info("Passage dans authentifier avec " + email + " " + password);
		if (!email.trim().isEmpty() && !password.trim().isEmpty()) {
			User u = this.ur.authentifier(email, password);
			UserController.LOG.info("test message user " + u);
			return u;
		} else {
			return null;
		}
	}

	@ResponseBody
	@RequestMapping(value = "/enregistrement", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public User save(@RequestParam("nom") String nom, @RequestParam("prenom") String prenom,
			@RequestParam("email") String email, @RequestParam("password") String password) {
		if (this.ur.existsByEmail(email)) {
			UserController.LOG.info("l'utilisateur existe bien " + email);
			return null;
			// retourner un message user existe deja voulez vous aller à la page de login?
		} else {
			UserController.LOG.info("l'utilisateur n'existe pas, on va l'ajouter à la BDD" + email);
			return this.ur.save(new User(nom, prenom, email, password));

		}
	}

}