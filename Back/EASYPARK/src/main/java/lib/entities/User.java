package lib.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "utilisateur")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_utilisateur")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idUser;
	@Column
	@NotNull
	private String nom;
	@Column
	@NotNull
	private String prenom;
	@Column
	@NotNull
	private String email;
	@Column
	@NotNull
	private String password;

	public User() {

	}

	// constructeur pour delete
	public User(int idUser) {
		this.idUser = idUser;
	}

	// constructeur pour create et update
	public User(String nom, String prenom, String email, String password) {
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.password = password;
	}

	public User(String email, String password) {
		this.email = email;
		this.password = password;
	}

	// constructeur du login
	public User(String nom, String prenom, String email) {
		this.email = email;
		this.nom = nom;
		this.prenom = prenom;
	}

	public User(int idUser, String nom, String prenom, String email, String password) {
		super();
		this.idUser = idUser;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.password = password;
	}

	public int getIdUser() {
		return this.idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static long getSerialversionuid() {
		return User.serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User [idUser=");
		builder.append(this.idUser);
		builder.append(", nom=");
		builder.append(this.nom);
		builder.append(", prenom=");
		builder.append(this.prenom);
		builder.append(", email=");
		builder.append(this.email);
		builder.append(", password=");
		builder.append(this.password);
		builder.append("]");
		return builder.toString();
	}

}