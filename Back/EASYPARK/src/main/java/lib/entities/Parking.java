package lib.entities;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Parking {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_parking")
	private int idParking;
	@Column
	@NotNull
	private String nom;
	@Column(name = "place_total")
	@NotNull
	private int placeTotal;

	@JsonIgnore
	@OneToMany(mappedBy = "parking")
	private Collection<Place> places;

	public Parking() {

	}

	public Parking(String nom, int placeTotal) {
		super();
		this.nom = nom;
		this.placeTotal = placeTotal;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPlaceTotal() {
		return this.placeTotal;
	}

	public void setPlaceTotal(int placeTotal) {
		this.placeTotal = placeTotal;
	}

	public Collection<Place> getPlaces() {
		return this.places;
	}

	public void setPlaces(Collection<Place> places) {
		this.places = places;
	}

	public int getIdParking() {
		return this.idParking;
	}

	public void setIdParking(int idParking) {
		this.idParking = idParking;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Parking [nom=");
		builder.append(this.nom);
		builder.append(", placeTotal=");
		builder.append(this.placeTotal);
		// builder.append(", places=");
		// builder.append(this.places);
		builder.append("]");
		return builder.toString();
	}

}
