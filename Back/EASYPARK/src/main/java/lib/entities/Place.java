package lib.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Place {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_place")
	private int idPlace;
	@Column
	@NotNull
	private boolean empty;
	@Column
	@NotNull
	private double longitude;
	@Column
	@NotNull
	private double latitude;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "parking_id_parking", referencedColumnName = "id_parking")
	private Parking parking;

	public Place() {

	}

	public Place(int idPlace, boolean empty, double longitude, double latitude, Parking parkings) {
		super();
		this.idPlace = idPlace;
		this.empty = empty;
		this.longitude = longitude;
		this.latitude = latitude;
		this.parking = parkings;
	}

	public int getIdPlace() {
		return this.idPlace;
	}

	public void setIdPlace(int idPlace) {
		this.idPlace = idPlace;
	}

	public boolean isEmpty() {
		return this.empty;
	}

	public void setEmpty(boolean empty) {
		this.empty = empty;
	}

	public double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public Parking getParking() {
		return this.parking;
	}

	public void setParking(Parking parkings) {
		this.parking = parkings;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Place [idPlace=");
		builder.append(this.idPlace);
		builder.append(", empty=");
		builder.append(this.empty);
		builder.append(", longitude=");
		builder.append(this.longitude);
		builder.append(", latitude=");
		builder.append(this.latitude);
		// builder.append(", parkings=");
		// builder.append(this.parkings);
		builder.append("]");
		return builder.toString();
	}

}
