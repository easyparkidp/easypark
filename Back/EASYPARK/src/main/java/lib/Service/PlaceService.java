// package lib.Service;
//
// import java.util.ArrayList;
// import java.util.List;
//
// import org.springframework.stereotype.Service;
// import org.springframework.web.bind.annotation.PathVariable;
//
// import lib.entities.Place;
//
// @Service
// public class PlaceService {
//
// public List<Place> getAllPlaces() {
//
// List<Place> places = new ArrayList<>();
//
// Place p1 = new Place();
// p1.setIdPlace(1);
// p1.setLatitude(1.23456465);
// p1.setLongitude(46.5764654);
// p1.setEmpty(false);
// places.add(p1);
//
// Place p2 = new Place();
// p2.setEmpty(false);
// places.add(p2);
//
// Place p3 = new Place();
// p3.setIdPlace(3);
// p3.setLatitude(144.23456465);
// p3.setLongitude(453.5764654);
// p3.setEmpty(true);
// places.add(p3);
//
// return places;
// }
//
// public Place getPlaceByID(@PathVariable("id") long id) {
// Place returnPlace = null;
//
// List<Place> places = this.getAllPlaces();
// for (Place p : places) {
// int myIdPlace = p.getIdPlace();
// if (myIdPlace == id) {
// returnPlace = p;
// break;
// }
// }
// return returnPlace;
// }
//
// }
