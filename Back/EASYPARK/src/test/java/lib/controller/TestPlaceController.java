package lib.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lib.DemoApplication;
import lib.entities.Place;
import lib.repositories.PlaceRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class TestPlaceController {

	private PlaceController placeController;

	public PlaceController getplaceController() {
		return this.placeController;
	}

	@Autowired
	public void setplaceController(PlaceController placeController, PlaceRepository pl) {
		this.placeController = placeController;
		this.placeController.setPl(pl);
	}

	// recuperation des parkings et des places disponibles
	@Before
	public void init() {
		int idparking = 1;
		boolean empty = false;
		this.placeController.getAllPlacesEmpty(empty, idparking);
	}

	/**
	 * Instancie une Place.
	 *
	 * @return une Place.
	 */

	private Place createPlace() {
		Place p1 = new Place();
		p1.setIdPlace(1);
		boolean empty = false;
		p1.setEmpty(empty);
		return p1;
	}

	/** Test. */
	@Test
	public void insertOk() {
		Place p1 = this.createPlace();
		boolean empty = false;
		int idparking = 1;
		this.placeController.getAllPlacesEmpty(empty, idparking);
	}

	/** Test. */
	@Test
	public void updateOk() {
		Place p1 = this.createPlace();
		boolean empty = false;
		int idparking = 0;
		this.placeController.getAllPlacesEmpty(empty, idparking);
		final String p = "nouvelle place";
		p1.setIdPlace(0);
		this.placeController.getAllPlacesEmpty(empty, idparking);
	}

	@After
	public void reset() {
		this.placeController.setPl(null);

	}
}