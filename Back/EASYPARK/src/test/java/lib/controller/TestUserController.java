package lib.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lib.DemoApplication;
import lib.entities.User;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class TestUserController {
	@Autowired
	private UserController userController;

	/**
	 * Instancie un utilisateur.
	 *
	 * @return un utilisateur sans ID.
	 */
	private User createUtilisateur() {
		User u1 = new User();
		u1.setNom("Smith-test");
		u1.setPrenom("Jhon-test");
		u1.setEmail("mail@test.com");
		u1.setPassword("bonjour");

		return u1;
	}

	/** Test. */
	@Test
	public void insertOk() {
		User u1 = this.createUtilisateur();
		User u2 = this.userController.save(u1.getNom(), u1.getPrenom(), u1.getEmail(), u1.getPassword());
		// Assert.assertEquals("Les deux utilisateurs ne doivent pas avoir le meme nom",
		// u1.getNom(), u2.getNom());
		// Assert.assertNotNull("L'utilisateur ne doit pas etre null", u2);
		Assert.assertNotSame("Les deux utilisateurs ne doivent pas avoir le meme nom et prénom", u2, u1);
		// Assert.assertNotSame("Ce mail est déjà enregistré", u1.getEmail(),
		// u2.getEmail());

	}

	/** Test. */
	@Test
	public void updateOk() {
		User u1 = this.createUtilisateur();
		User u2 = this.userController.save(u1.getNom(), u1.getPrenom(), u1.getEmail(), u1.getPassword());
		if (u2 == null) {
			System.out.println("utilisateur trouvé");
			return;
		}
		final String p = "nouveau prénom";
		u2.setPrenom(p);
		User u3 = this.userController.save(u2.getNom(), u2.getPrenom(), u2.getEmail(), u2.getPassword());
		// Assert.assertNotNull("L'utilisateur ne doit pas etre null", u3);
		Assert.assertNotNull("L'utilisateur doit avoir un nom", u3.getNom());
		Assert.assertEquals("Les deux utilisateurs doivent avoir le meme nom", u2.getNom(), u3.getNom());
		Assert.assertEquals("Le prenom doit etre " + p, p, u3.getPrenom());
		Assert.assertEquals("Les deux utilisateurs doivent avoir un prenom different", u2.getPrenom(), u3.getPrenom());
	}
}