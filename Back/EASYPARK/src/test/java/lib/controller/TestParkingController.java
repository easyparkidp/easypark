package lib.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lib.DemoApplication;
import lib.entities.Parking;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class TestParkingController {

	@Autowired
	private ParkingController parkingController;

	/**
	 * Instancie un parking.
	 *
	 * @return un parking sans ID.
	 */
	private Parking createParking() {
		Parking p1 = new Parking();
		p1.setNom("Auchan");
		p1.setPlaceTotal(500);
		return p1;
	}

	/** Test. */
	@Test
	public void insertOk() {
		Parking p1 = this.createParking();
		String nom = "Auchan";
		this.parkingController.envoyer(nom);
	}

	/** Test. */
	@Test
	public void updateOk() {
		Parking p1 = this.createParking();
		String nom = "Auchan";
		this.parkingController.envoyer(nom);
		final String p = "nouveau parking";
		p1.setNom(p);
		this.parkingController.envoyer(nom);
	}

}